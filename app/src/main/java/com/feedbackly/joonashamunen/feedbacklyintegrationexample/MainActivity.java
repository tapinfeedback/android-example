package com.feedbackly.joonashamunen.feedbacklyintegrationexample;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WebView myWebView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        myWebView.addJavascriptInterface(new WebAppInterface(this), "androidAppProxy");

        String CHANNEL_ID = "BJekU0gi_e"; // this is an example id

        myWebView.loadUrl("https://survey.feedbackly.com/surveys/" + CHANNEL_ID + "?decorators=plugin"
               // + ",popup" // add this if you're displaying the survey in 1/3 or 1/2 screen
        );


    }

    public class WebAppInterface {
        Context mContext;

        /** Instantiate the interface and set the context */
        WebAppInterface(Context c) {
            mContext = c;
        }

        /** Show a toast from the web page */
        @JavascriptInterface
        public void showMessage(String msg) {
            try {
                JSONObject mainObject = new JSONObject(msg);
                String name = mainObject.getString("action");

                if(name == "surveyLoaded"){
                    Log.d("FBLY", "Survey is loaded.");
                    // code to make webview visible
                }
                if(name == "surveyFinished"){
                    Log.d("FBLY", "Survey is finished.");
                    // hide webview
                }

            }catch(JSONException e){};

        }
    }


}
